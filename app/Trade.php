<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'type',
        'network',
        'ad_id',
        'trader',
        'payment_hash',
        'crypto_price',
        'crypto_amount',
        'amount',
        'payment_instructions',
        'status',
        'created',
        'timeout',
        'released_bitcoins',
        'referral_id',
        'attachment',
    ];

}
