<?php

namespace App\Http\Controllers;

use App\PostAdd;
use Illuminate\Http\Request;
use Auth;


class AdsController extends Controller
{
    public function advertisement()
    {
        $title = "Advertisement";
        $loginUser = Auth::user();
        $model = PostAdd::where('uid',$loginUser->id)->get();
        return view('account.advertisement')
            ->with('title',$title)
            ->with('model',$model);
    }

    public function advertisementEdit($id)
    {
        $postAdd = PostAdd::find($id);

        $title = 'Edit Advertisement';
        return view('account.advertisement-edit')
            ->with('id',$id)
            ->with('title',$title)
            ->with('model',$postAdd);

    }

    public function advertisementUpdate(Request $request,$id)
    {
        $type = $request->get('type');

        $network = 'Bitcoin';
        if($type == "buy_litecoin" || $type == "sell_litecoin") {
            $network = 'Litecoin';
        }elseif($type == "buy_dogecoin" or $type == "sell_dogecoin"){
            $network = 'Dogecoin';
        }

        $require_document = 0;
        if($request->has('require_document')){
            $require_document = $request->get('require_document');
            if($require_document == 'yes'){
                $require_document = 1;
            }
        }

        $require_email = 0;
        if($request->has('require_email')){
            $require_email = $request->get('require_email');
            if($require_email == 'yes'){
                $require_email = 1;
            }
        }

        $require_mobile = 0;
        if($request->has('require_mobile')){
            $require_mobile = $request->get('require_mobile');
            if($require_mobile == 'yes'){
                $require_mobile = 1;
            }
        }

        $request->request->add([
            'uid' => Auth::user()->id,
            'type' => $type,
            'network' => $network,
            'require_document' => $require_document,
            'require_email' => $require_email,
            'require_mobile' => $require_mobile,
        ]);

        $this->validator($request->all())->validate();
        $request->request->add(['price'=>$request->amount]);

        $postAdd = PostAdd::find($id);

        $postAdd->update($request->all());

        $title = 'Edit Advertisement';
        return back()
            ->with('id',$id)
            ->with('title',$title)
            ->with('alert_class', 'success')
            ->with('model',$postAdd)
            ->with('flash-message', __('crypto.success_12'));
    }

    public function advertisementDeleteConfirmation($id)
    {
        $title = 'Delete Advertisement';
        return view('account.advertisement-delete-confirmation')
            ->with('id',$id)
            ->with('title',$title)
            ->with('action','confirmation');
    }

    public function advertisementDelete($id)
    {
        $model = PostAdd::find($id)->delete();

        $title = 'Delete Advertisement';
        return view('account.advertisement-delete-confirmation')
            ->with('id',$id)
            ->with('title',$title)
            ->with('action','deleted');
    }

}
