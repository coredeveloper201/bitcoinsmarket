<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradeMessage extends Model
{
    protected $table = 'trades_messages';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'trade_id',
        'message',
        'attachment',
        'readed',
        'payment_instructions',
        'time',
    ];

}
