<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersAddress extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'network',
        'label',
        'address',
        'lid',
        'lid',
        'available_balance',
        'pending_received_balance',
        'status',
        'created',
        'updated',
    ];

}
