@extends('layouts.master')
@section('content')

	@if($model->type == "buy_bitcoin")
		@include('post-ad-buy-btc')
    @elseif($model->type == "sell_bitcoin")
		@include('post-ad-sell-btc')
    @elseif($model->type == "buy_litecoin")
        @include('post-ad-buy-ltc')
    @elseif($model->type == "sell_litecoin")
        @include('post-ad-sell-ltc')
    @elseif($model->type == "buy_dogecoin")
        @include('post-ad-buy-dogec')
    @elseif($model->type == "sell_dogecoin")
        @include('post-ad-sell-dogec')
	@endif
@endsection