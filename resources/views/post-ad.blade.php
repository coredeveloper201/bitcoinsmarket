@extends('layouts.master')
@section('content')

    <div class="container-fluid page-title">
			<div class="row blue-banner">
            	<div class="container main-container">
                	<div class="col-lg-12 col-md-12 col-sm-12">
                		<h3 class="white-heading">@lang('crypto.post_ad')</h3>
                    </div>
                </div>
            </div>
        </div>
  	 <!--header section -->

    <!-- full width section -->
    	<div class="container-fluid white-bg">
        	<div class="row">
            	<div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">

							@if(count($errors))
								<div class="alert alert-danger">
									@foreach($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</div>
							@endif
							@if(session('flash-message'))
								 <div class="alert alert-{{ session('alert_class') }}">
									 <i class="fa fa-check"></i>  {{ session('flash-message') }}
									 	@if(session('post_id'))
										 	<a href="{{route('postAdView',session('post_id'))}}" target="_blank">Click Here</a>
									 	@endif
								</div>
							@endif

					<form action="{{ route('storePostAd') }}" method="POST">
						    {{ csrf_field() }}
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>@lang('crypto.ad_type')</label>
									@if(getSettingsData()->bitcoin_status)
									<br>
									<div class="radio">
									  <label>
										<input type="radio" name="type" id="ad_type" value="buy_bitcoin" onchange="change_ad_type(this.value);" checked>
										@lang('crypto.buy_bitcoins')
									  </label>
									</div>
									<br>
									<div class="radio">
									  <label>
										<input type="radio" name="type" id="ad_type" value="sell_bitcoin" onchange="change_ad_type(this.value);">
										@lang('crypto.sell_bitcoins')
									  </label>
									</div>
									@endif
									@if(getSettingsData()->litecoin_status)
									<br>
									<div class="radio">
									  <label>
										<input type="radio" name="type" id="ad_type" value="buy_litecoin" onchange="change_ad_type(this.value);">
										@lang('crypto.buy_litecoin')
									  </label>
									</div>
									<br>
									<div class="radio">
									  <label>
										<input type="radio" name="type" id="ad_type" value="sell_litecoin" onchange="change_ad_type(this.value);">
										@lang('crypto.sell_litecoin')
									  </label>
									</div>
									@endif
									@if(getSettingsData()->dogecoin_status)
									<br>
									<div class="radio">
									  <label>
										<input type="radio" name="type" id="ad_type" value="buy_dogecoin" onchange="change_ad_type(this.value);">
										@lang('crypto.buy_dogecoin')
									  </label>
									</div>
									<br>
									<div class="radio">
									  <label>
										<input type="radio" name="type" id="ad_type" value="sell_dogecoin" onchange="change_ad_type(this.value);">
										@lang('crypto.sell_dogecoin')
									  </label>
									</div>
									@endif
								</div>
							</div>
							<input type="hidden" id="ad_type2" value="buy_bitcoin">
							<div class="col-md-6">
								<br><span class="text text-muted">@lang('crypto.ad_type_info')</span>
							</div>
							<div class="col-md-12"><br></div>
							<div class="col-md-6">
								<div class="form-group">
									<label>@lang('crypto.payment_method')</label>
									<select class="form-control" name="payment_method">
										<option value="PayPal">PayPal</option>
										<option value="Skrill">Skrill</option>
										<option value="Payeer">Payeer</option>
										<option value="Xoomwallet">Xoomwallet</option>
										<option value="Perfect Money">Perfect Money</option>
										<option value="Payoneer">Payoneer</option>
										<option value="AdvCash">AdvCash</option>
										<option value="OKPay">OKPay</option>
										<option value="Entromoney">Entromoney</option>
										<option value="SolidTrust Pay">SolidTrust Pay</option>
										<option value="Neteller">Neteller</option>
										<option value="UQUID">UQUID</option>
										<option value="Yandex Money">Yandex Money</option>
										<option value="QIWI">QIWI</option>
										<option value="Bank Transfer">Bank Transfer</option>
										<option value="Western Union">Western Union</option>
										<option value="Moneygram">Moneygram</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<br><span class="text text-muted">@lang('crypto.payment_method_info')</span>
							</div>
							<div class="col-md-12"><br></div>
							<div class="col-md-6">
								<div class="form-group">
									<label>@lang('crypto.currency')</label>
									<select class="form-control" name="currency" onchange="btc_calculate_price();" id="btc_currency">
										<option value="AED">AED - United Arab Emirates Dirham</option>
											<option value="AFN">AFN - Afghanistan Afghani</option>
											<option value="ALL">ALL - Albania Lek</option>
											<option value="AMD">AMD - Armenia Dram</option>
											<option value="ANG">ANG - Netherlands Antilles Guilder</option>
											<option value="AOA">AOA - Angola Kwanza</option>
											<option value="ARS">ARS - Argentina Peso</option>
											<option value="AUD">AUD - Australia Dollar</option>
											<option value="AWG">AWG - Aruba Guilder</option>
											<option value="AZN">AZN - Azerbaijan New Manat</option>
											<option value="BAM">BAM - Bosnia and Herzegovina Convertible Marka</option>
											<option value="BBD">BBD - Barbados Dollar</option>
											<option value="BDT">BDT - Bangladesh Taka</option>
											<option value="BGN">BGN - Bulgaria Lev</option>
											<option value="BHD">BHD - Bahrain Dinar</option>
											<option value="BIF">BIF - Burundi Franc</option>
											<option value="BMD">BMD - Bermuda Dollar</option>
											<option value="BND">BND - Brunei Darussalam Dollar</option>
											<option value="BOB">BOB - Bolivia Boliviano</option>
											<option value="BRL">BRL - Brazil Real</option>
											<option value="BSD">BSD - Bahamas Dollar</option>
											<option value="BTN">BTN - Bhutan Ngultrum</option>
											<option value="BWP">BWP - Botswana Pula</option>
											<option value="BYR">BYR - Belarus Ruble</option>
											<option value="BZD">BZD - Belize Dollar</option>
											<option value="CAD">CAD - Canada Dollar</option>
											<option value="CDF">CDF - Congo/Kinshasa Franc</option>
											<option value="CHF">CHF - Switzerland Franc</option>
											<option value="CLP">CLP - Chile Peso</option>
											<option value="CNY">CNY - China Yuan Renminbi</option>
											<option value="COP">COP - Colombia Peso</option>
											<option value="CRC">CRC - Costa Rica Colon</option>
											<option value="CUC">CUC - Cuba Convertible Peso</option>
											<option value="CUP">CUP - Cuba Peso</option>
											<option value="CVE">CVE - Cape Verde Escudo</option>
											<option value="CZK">CZK - Czech Republic Koruna</option>
											<option value="DJF">DJF - Djibouti Franc</option>
											<option value="DKK">DKK - Denmark Krone</option>
											<option value="DOP">DOP - Dominican Republic Peso</option>
											<option value="DZD">DZD - Algeria Dinar</option>
											<option value="EGP">EGP - Egypt Pound</option>
											<option value="ERN">ERN - Eritrea Nakfa</option>
											<option value="ETB">ETB - Ethiopia Birr</option>
											<option value="EUR">EUR - Euro Member Countries</option>
											<option value="FJD">FJD - Fiji Dollar</option>
											<option value="FKP">FKP - Falkland Islands (Malvinas) Pound</option>
											<option value="GBP">GBP - United Kingdom Pound</option>
											<option value="GEL">GEL - Georgia Lari</option>
											<option value="GGP">GGP - Guernsey Pound</option>
											<option value="GHS">GHS - Ghana Cedi</option>
											<option value="GIP">GIP - Gibraltar Pound</option>
											<option value="GMD">GMD - Gambia Dalasi</option>
											<option value="GNF">GNF - Guinea Franc</option>
											<option value="GTQ">GTQ - Guatemala Quetzal</option>
											<option value="GYD">GYD - Guyana Dollar</option>
											<option value="HKD">HKD - Hong Kong Dollar</option>
											<option value="HNL">HNL - Honduras Lempira</option>
											<option value="HPK">HRK - Croatia Kuna</option>
											<option value="HTG">HTG - Haiti Gourde</option>
											<option value="HUF">HUF - Hungary Forint</option>
											<option value="IDR">IDR - Indonesia Rupiah</option>
											<option value="ILS">ILS - Israel Shekel</option>
											<option value="IMP">IMP - Isle of Man Pound</option>
											<option value="INR">INR - India Rupee</option>
											<option value="IQD">IQD - Iraq Dinar</option>
											<option value="IRR">IRR - Iran Rial</option>
											<option value="ISK">ISK - Iceland Krona</option>
											<option value="JEP">JEP - Jersey Pound</option>
											<option value="JMD">JMD - Jamaica Dollar</option>
											<option value="JOD">JOD - Jordan Dinar</option>
											<option value="JPY">JPY - Japan Yen</option>
											<option value="KES">KES - Kenya Shilling</option>
											<option value="KGS">KGS - Kyrgyzstan Som</option>
											<option value="KHR">KHR - Cambodia Riel</option>
											<option value="KMF">KMF - Comoros Franc</option>
											<option value="KPW">KPW - Korea (North) Won</option>
											<option value="KRW">KRW - Korea (South) Won</option>
											<option value="KWD">KWD - Kuwait Dinar</option>
											<option value="KYD">KYD - Cayman Islands Dollar</option>
											<option value="KZT">KZT - Kazakhstan Tenge</option>
											<option value="LAK">LAK - Laos Kip</option>
											<option value="LBP">LBP - Lebanon Pound</option>
											<option value="LKR">LKR - Sri Lanka Rupee</option>
											<option value="LRD">LRD - Liberia Dollar</option>
											<option value="LSL">LSL - Lesotho Loti</option>
											<option value="LYD">LYD - Libya Dinar</option>
											<option value="MAD">MAD - Morocco Dirham</option>
											<option value="MDL">MDL - Moldova Leu</option>
											<option value="MGA">MGA - Madagascar Ariary</option>
											<option value="MKD">MKD - Macedonia Denar</option>
											<option value="MMK">MMK - Myanmar (Burma) Kyat</option>
											<option value="MNT">MNT - Mongolia Tughrik</option>
											<option value="MOP">MOP - Macau Pataca</option>
											<option value="MRO">MRO - Mauritania Ouguiya</option>
											<option value="MUR">MUR - Mauritius Rupee</option>
											<option value="MVR">MVR - Maldives (Maldive Islands) Rufiyaa</option>
											<option value="MWK">MWK - Malawi Kwacha</option>
											<option value="MXN">MXN - Mexico Peso</option>
											<option value="MYR">MYR - Malaysia Ringgit</option>
											<option value="MZN">MZN - Mozambique Metical</option>
											<option value="NAD">NAD - Namibia Dollar</option>
											<option value="NGN">NGN - Nigeria Naira</option>
											<option value="NTO">NIO - Nicaragua Cordoba</option>
											<option value="NOK">NOK - Norway Krone</option>
											<option value="NPR">NPR - Nepal Rupee</option>
											<option value="NZD">NZD - New Zealand Dollar</option>
											<option value="OMR">OMR - Oman Rial</option>
											<option value="PAB">PAB - Panama Balboa</option>
											<option value="PEN">PEN - Peru Nuevo Sol</option>
											<option value="PGK">PGK - Papua New Guinea Kina</option>
											<option value="PHP">PHP - Philippines Peso</option>
											<option value="PKR">PKR - Pakistan Rupee</option>
											<option value="PLN">PLN - Poland Zloty</option>
											<option value="PYG">PYG - Paraguay Guarani</option>
											<option value="QAR">QAR - Qatar Riyal</option>
											<option value="RON">RON - Romania New Leu</option>
											<option value="RSD">RSD - Serbia Dinar</option>
											<option value="RUB">RUB - Russia Ruble</option>
											<option value="RWF">RWF - Rwanda Franc</option>
											<option value="SAR">SAR - Saudi Arabia Riyal</option>
											<option value="SBD">SBD - Solomon Islands Dollar</option>
											<option value="SCR">SCR - Seychelles Rupee</option>
											<option value="SDG">SDG - Sudan Pound</option>
											<option value="SEK">SEK - Sweden Krona</option>
											<option value="SGD">SGD - Singapore Dollar</option>
											<option value="SHP">SHP - Saint Helena Pound</option>
											<option value="SLL">SLL - Sierra Leone Leone</option>
											<option value="SOS">SOS - Somalia Shilling</option>
											<option value="SRL">SPL* - Seborga Luigino</option>
											<option value="SRD">SRD - Suriname Dollar</option>
											<option value="STD">STD - Sao Tome and Principe Dobra</option>
											<option value="SVC">SVC - El Salvador Colon</option>
											<option value="SYP">SYP - Syria Pound</option>
											<option value="SZL">SZL - Swaziland Lilangeni</option>
											<option value="THB">THB - Thailand Baht</option>
											<option value="TJS">TJS - Tajikistan Somoni</option>
											<option value="TMT">TMT - Turkmenistan Manat</option>
											<option value="TND">TND - Tunisia Dinar</option>
											<option value="TOP">TOP - Tonga Pa'anga</option>
											<option value="TRY">TRY - Turkey Lira</option>
											<option value="TTD">TTD - Trinidad and Tobago Dollar</option>
											<option value="TVD">TVD - Tuvalu Dollar</option>
											<option value="TWD">TWD - Taiwan New Dollar</option>
											<option value="TZS">TZS - Tanzania Shilling</option>
											<option value="UAH">UAH - Ukraine Hryvnia</option>
											<option value="UGX">UGX - Uganda Shilling</option>
											<option value="USD" selected>USD - United States Dollar</option>
											<option value="UYU">UYU - Uruguay Peso</option>
											<option value="UZS">UZS - Uzbekistan Som</option>
											<option value="VEF">VEF - Venezuela Bolivar</option>
											<option value="VND">VND - Viet Nam Dong</option>
											<option value="VUV">VUV - Vanuatu Vatu</option>
											<option value="WST">WST - Samoa Tala</option>
											<option value="XAF">XAF - Communaute Financiere Africaine (BEAC) CFA Franc BEAC</option>
											<option value="XCD">XCD - East Caribbean Dollar</option>
											<option value="XDR">XDR - International Monetary Fund (IMF) Special Drawing Rights</option>
											<option value="XOF">XOF - Communaute Financiere Africaine (BCEAO) Franc</option>
											<option value="XPF">XPF - Comptoirs Francais du Pacifique (CFP) Franc</option>
											<option value="YER">YER - Yemen Rial</option>
											<option value="ZAR">ZAR - South Africa Rand</option>
											<option value="ZMW">ZMW - Zambia Kwacha</option>
											<option value="ZWD">ZWD - Zimbabwe Dollar</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<br><span class="text text-muted">@lang('crypto.currency_info')</span>
							</div>
							<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
										<label>@lang('crypto.enter_comission')</label>
										<input type="text" class="form-control" name="amount" onkeyup="btc_calculate_price();" id="btc_amount">
										@lang('crypto.currenct_bitcoin_price'): <span id="btc_price">{{get_current_bitcoin_price()}}</span><span id="ltc_price" style="display:none;">{{get_current_litecoin_price()}}</span><span id="doge_price" style="display:none;">{{get_current_dogecoin_price()}}</span> USD<br/>
										<span id="your_btc_price"></span>
										@if ($errors->has('amount'))
											<span class="help-block">
												<strong>{{ $errors->first('amount') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<br><span class="text text-muted">@lang('crypto.enter_comission_info')</span>
								</div>
							<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group{{ $errors->has('payment_instructions') ? ' has-error' : '' }}">
										<label>@lang('crypto.payment_instructions')</label>
										<textarea class="form-control" name="payment_instructions" rows="5"></textarea>
										@if ($errors->has('payment_instructions'))
											<span class="help-block">
												<strong>{{ $errors->first('payment_instructions') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<br><span class="text text-muted">@lang('crypto.payment_instructions_info')</span>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group{{ $errors->has('min_amount') ? ' has-error' : '' }}">
										<label>@lang('crypto.minimal_trade_amount')</label>
										<input type="text" class="form-control" name="min_amount">
										@if ($errors->has('min_amount'))
											<span class="help-block">
												<strong>{{ $errors->first('min_amount') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<br><span class="text text-muted">@lang('crypto.minimal_trade_amount_info')</span>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group{{ $errors->has('max_amount') ? ' has-error' : '' }}">
										<label>@lang('crypto.maximum_trade_amount')</label>
										<input type="text" class="form-control" name="max_amount">
										@if ($errors->has('max_amount'))
											<span class="help-block">
												<strong>{{ $errors->first('max_amount') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<br><span class="text text-muted">@lang('crypto.maximum_trade_amount_info')</span>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group{{ $errors->has('process_time') ? ' has-error' : '' }}">
										<label>@lang('crypto.trade_process_time')</label>
										<input type="text" class="form-control" name="process_time">
										@if ($errors->has('process_time'))
											<span class="help-block">
												<strong>{{ $errors->first('process_time') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<br><span class="text text-muted">@lang('crypto.trade_process_time_info')</span>
								</div>
								<div class="col-md-12"><br></div>
								<div class="col-md-6">
									<div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }}">
										<label>@lang('crypto.terms_of_trade')</label>
										<textarea class="form-control" name="terms" rows="5"></textarea>
										@if ($errors->has('terms'))
											<span class="help-block">
												<strong>{{ $errors->first('terms') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
									<br><span class="text text-muted">@lang('crypto.terms_of_trade_info')</span>
								</div>
								@if(getSettingsData()->document_verification)
								<div class="col-md-12">
									 <div class="checkbox">
										<label>
										  <input type="checkbox" name="require_document" value="yes"> @lang('crypto.require_document')
										</label>
									  </div>
								</div>
								@endif
								@if(getSettingsData()->email_verification)
								<div class="col-md-12">
									 <div class="checkbox">
										<label>
										  <input type="checkbox" name="require_email" value="yes"> @lang('crypto.require_email')
										</label>
									  </div>
								</div>
								@endif
								@if(getSettingsData()->phone_verification)
								<div class="col-md-12">
									 <div class="checkbox">
										<label>
										  <input type="checkbox" name="require_mobile" value="yes"> @lang('crypto.require_mobile')
										</label>
									  </div>
								</div>
								@endif
								<div class="col-md-12">
									<button type="submit" class="btn btn-warning" >@lang('crypto.btn_submit')</button>
								</div>
						</div>
					</form>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection