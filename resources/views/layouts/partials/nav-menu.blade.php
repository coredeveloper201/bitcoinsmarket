<!-- Header Image Or May be Slider-->
<div id="header" class="container-fluid pages">
      <div class="row">
     <!-- Header Image Or May be Slider-->
        <div class="top_header">
            <nav class="navbar navbar-fixed-top">

                 <div class="container">
                     <input type="hidden" value="{{url('/')}}" id="url">
                     <div class="logo">
                        <a href="{{url('/')}}"><img src="{{asset('assets/images/logo2.png')}}" style="margin-top:-11px;" alt="logo"/></a>
                     </div>
                     
                     <div class="logins">
                        <a href="{{route('postAd')}}" class="post_job"><span class="label job-type partytime">@lang('crypto.post_ad')</span></a>

                        @if($signed_in)
                            <a href="{{route('logout')}}" class="login"><i class="fa fa-sign-out"></i></a>
                            <a href="{{route('account.wallet')}}" class="login"><i class="fa fa-user"></i></a>
                        @else
                            <a href="{{route('login')}}" class="login"><i class="fa fa-user"></i></a>
                        @endif
                    </div>
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                          <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">buy<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        @if(getSettingsData()->bitcoin_status)
                                            <li><a href="{{route('buyBitcoin')}}">@lang('crypto.bitcoin')</a></li>
                                        @endif

                                        @if(getSettingsData()->litecoin_status)
                                            <li><a href="{{route('buyLitecoin')}}">@lang('crypto.litecoin')</a></li>
                                        @endif

                                        @if(getSettingsData()->dogecoin_status)
                                            <li><a href="{{route('buyDogecoin')}}">@lang('crypto.dogecoin')</a></li>
                                        @endif
                                  </ul>
                                </li>
                                 <li class="dropdown">
                                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">sell<span class="caret"></span></a>

                                     <ul class="dropdown-menu">
                                         @if(getSettingsData()->bitcoin_status)
                                            <li><a href="{{route('sellBitcoin')}}">@lang('crypto.bitcoin')</a></li>
                                         @endif

                                         @if(getSettingsData()->litecoin_status)
                                            <li><a href="{{route('sellLitecoin')}}">@lang('crypto.litecoin')</a></li>
                                         @endif

                                         @if(getSettingsData()->dogecoin_status)
                                            <li><a href="{{route('sellDogecoin')}}">@lang('crypto.dogecoin')</a></li>
                                         @endif
                                     </ul>
                                 </li>
                                <lI><a href="{{route('contact')}}">@lang('crypto.contact')</a></li>

                                @if($signed_in)
                                    <li class="mobile-menu"><a href="{{route('account.wallet')}}">@lang('crypto.menu_wallet')</a></li>
                                    <li class="mobile-menu"><a href="{{route('logout')}}">@lang('crypto.menu_logout')</a></li>
                                @else
                                    <li class="mobile-menu"><a href="{{route('postAd')}}">@lang('crypto.post_ad')</a></li>
                                    <li class="mobile-menu"><a href="{{route('login')}}">@lang('crypto.login') / @lang('crypto.register')</a></li>
                                @endif
                          </ul>

                    </div><!-- navbar-collapse -->
                </div>
            <!-- container-fluid -->
            </nav>
         </div>
    </div>
</div>
<!-- Header Image Or May be Slider-->