@extends('layouts.master')
@section('content')
<!-- Page Title-->
	<div class="container-fluid blue-banner page-title bg-image">
	</div>
<!-- Page Title-->

	<div class="container ex_padding" style="padding-top:20px;padding-bottom:20px;font-size:15px;">
		<div class="row">
			<div class="col-md-3">
				@include('account.accordion-menu')
			</div>
			<div class="col-md-9">
				<div class="panel panel-default">
					<div class="panel-body">
						<h4>@lang('crypto.menu_advertisements')</h4>
						<hr/>
						@if(count($model) > 0)
							<table class="table table-striped">
								<thead>
									<tr>
										<th>@lang('crypto.ad_type')</th>
										<th>@lang('crypto.payment_method')</th>
										<th>@lang('crypto.price')</th>
										<th>@lang('crypto.limits')</th>
										<th>@lang('crypto.process_time')</th>
										<th>@lang('crypto.action')</th>
									</tr>
								</thead>
								<tbody>
									@foreach($model as $single)
										@php
											if($single->type == "buy_bitcoin") {
												$type = 'Buy Bitcoin';
												$ext = 'BTC';
												$crypto_price = convertBTCprice($single->price,$single->currency);
											} elseif($single->type == "sell_bitcoin") {
												$type = 'Sell Bitcoin';
												$ext = 'BTC';
												$crypto_price = convertBTCprice($single->price,$single->currency);
											} elseif($single->type == "buy_litecoin") {
												$type = 'Buy Litecoin';
												$ext = 'LTC';
												$crypto_price = convertLTCprice($single->price,$single->currency);
											} elseif($single->type == "sell_litecoin") {
												$type = 'Sell Litecoin';
												$ext = 'LTC';
												$crypto_price = convertLTCprice($single->price,$single->currency);
											} elseif($single->type == "buy_dogecoin") {
												$type = 'Buy Dogecoin';
												$ext = 'DOGE';
												$crypto_price = convertDOGEprice($single->price,$single->currency);
											} elseif($single->type == "sell_dogecoin") {
												$type = 'Sell Dogecoin';
												$ext = 'DOGE';
												$crypto_price = convertDOGEprice($single->price,$single->currency);
											} else { }
										@endphp

										<tr>
											<td>{{ $type }}</td>
											<td>{{ $single->payment_method }}</td>
											<td>{{ $crypto_price }} {{ $single->currency }} / {{ $ext }}</td>
											<td>{{ $single->min_amount }} - {{ $single->max_amount }}  {{ $single->currency }} </td>
											<td>{{ $single->process_time }}  minutes</td>
											<td>
												<a href="{{route('account.advertisementEdit',$single->id)}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
												<a href="{{route('account.advertisementDeleteConfirmation',$single->id)}}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@else
							<div class="alert alert-info"><i class="fa fa-info-circle"></i> @lang('crypto.info_5')</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection