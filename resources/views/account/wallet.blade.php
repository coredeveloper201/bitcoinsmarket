@extends('layouts.master')
@section('content')
    <!-- Page Title-->
    	<div class="container-fluid page-title dashboard ">
			<div class="row blue-banner">
            	<div class="container main-container">
                	<div class="col-md-4">
						<span style="color:#fff;font-size:35px;font-weight:bold;">@lang('crypto.av_balance')</span><br/>
						@if(getSettingsData()->bitcoin_status)
							<span style="color:#fff;font-size:25px;">
								<i class="cc BTC-alt"></i> {{get_user_balance($loginUser->id,'Bitcoin')}}
							</span><br/>
						@endif

						@if(getSettingsData()->litecoin_status)
							<span style="color:#fff;font-size:25px;">
								<i class="cc LTC-alt"></i> {{get_user_balance($loginUser->id,'Litecoin')}}
							</span><br/>
						@endif

						@if(getSettingsData()->dogecoin_status)
							<span style="color:#fff;font-size:25px;">
								<i class="cc DOGE-alt"></i> {{get_user_balance($loginUser->id,'Dogecoin')}}
							</span><br/>
						@endif

					</div>
					<div class="col-md-4">
						<span style="color:#fff;font-size:35px;font-weight:bold;">@lang('crypto.pe_balance')</span><br/>

						@if(getSettingsData()->bitcoin_status)
							<span style="color:#fff;font-size:25px;">
								<i class="cc BTC-alt"></i> {{walletinfo($loginUser->id,'pending_received_balance','Bitcoin')}}
							</span><br/>
						@endif
						@if(getSettingsData()->litecoin_status)
							<span style="color:#fff;font-size:25px;">
								<i class="cc LTC-alt"></i> {{walletinfo($loginUser->id,'pending_received_balance','Litecoin')}}
							</span><br/>
						@endif
						@if(getSettingsData()->dogecoin_status)
							<span style="color:#fff;font-size:25px;">
								<i class="cc DOGE-alt"></i> {{walletinfo($loginUser->id,'pending_received_balance','Dogecoin')}}
							</span><br/>
						@endif
					</div>

					<div class="col-md-4">

					</div>
                </div>
            </div>
        </div>
    <!-- Page Title-->



	<div class="container ex_padding" style="padding-top:20px;padding-bottom:20px;font-size:15px;">
		<div class="row">
			<div class="col-md-12">
				@include('flash-message')
			</div>
			<div class="col-md-4">

				<div class="panel panel-default">
					<div class="panel-body">
						<h4>@lang('crypto.deposit_receive_bitcoins')</h4>
						<p>@lang('crypto.d_r_bitcoins')</p>
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

							@if(getSettingsData()->bitcoin_status)
							  <div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
								  <h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									  Bitcoin
									</a>
								  </h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								  <div class="panel-body">
									<center>
									<img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{walletinfo($loginUser->id,"address","Bitcoin")}}&choe=UTF-8"><br/>
									<input type="text" class="form-control text-center" value="{{walletinfo($loginUser->id,"address","Bitcoin")}}"  onclick="this.select();">
									</center><br>
									</div>
								</div>
							  </div>
						  	@endif

						  @if(getSettingsData()->litecoin_status)
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								  Litecoin
								</a>
							  </h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							  <div class="panel-body">
								<center>
								<img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{walletinfo($loginUser->id,"address","Litecoin")}}&choe=UTF-8"><br/>
								<input type="text" class="form-control text-center" value="{{walletinfo($loginUser->id,"address","Litecoin")}}"  onclick="this.select();">
								</center><br>
								</div>
							</div>
						  </div>
						  @endif

						  @if(getSettingsData()->dogecoin_status)
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								  Dogecoin
								</a>
							  </h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							  <div class="panel-body">
								<center>
								<img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{walletinfo($loginUser->id,"address","Dogecoin")}}&choe=UTF-8"><br/>
								<input type="text" class="form-control text-center" value="{{walletinfo($loginUser->id,"address","Dogecoin")}}"  onclick="this.select();">
								</center><br>
								</div>
							</div>
						  </div>
						  @endif
						</div>

					</div>
				</div>

				@include('account.accordion-menu')

			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<h4>@lang('crypto.pending_trades')</h4>
						@lang('crypto.pending_trades_info')

						@include('account.pending-trades')

					</div>
				</div>

				<div class="panel panel-default panel-blue">
					<div class="panel-body">
						<h4>@lang('crypto.send_bitcoins')</h4>
						<form action="{{route('account.walletSend')}}" method="POST">
							{{csrf_field()}}
							<div class="form-group">
								<label>@lang('crypto.from_wallet')</label>
								<select class="form-control" name="from_wallet" onchange="refresh_txfee(this.value);">
									@if(getSettingsData()->bitcoin_status)
										<option value="Bitcoin">Bitcoin - {{walletinfo($loginUser->id,"available_balance","Bitcoin")}} BTC</option>
									@endif
									@if(getSettingsData()->litecoin_status)
										<option value="Litecoin">Litecoin - {{walletinfo($loginUser->id,"available_balance","Litecoin")}} LTC</option>
									@endif
									@if(getSettingsData()->dogecoin_status)
										<option value="Dogecoin">Dogecoin - {{walletinfo($loginUser->id,"available_balance","Dogecoin")}} DOGE</option>
									@endif
								</select>
							</div>
							<div class="form-group">
								<label>@lang('crypto.amount')</label>
								<input type="text" class="form-control" name="amount" placeholder="0.0000000">
							</div>
							<div class="form-group">
								<label>@lang('crypto.recipient')</label>
								<input type="text" class="form-control" name="recipient">
							</div>
							<button type="submit" class="btn btn-warning" name="btc_send_bitcoins">@lang('crypto.btn_send')</button>
							<span class="pull-right" id="bitcoin_txfee">@lang('crypto.minimal_transaction_fee'): {{getSettingsData()->bitcoin_withdrawal_comission}}+0.0008; BTC</span>
							<span class="pull-right" id="litecoin_txfee" style="display:none;">@lang('crypto.minimal_transaction_fee'): {{getSettingsData()->litecoin_withdrawal_comission}}+0.005 LTC</span>
							<span class="pull-right" id="dogecoin_txfee" style="display:none;">@lang('crypto.minimal_transaction_fee'): {{getSettingsData()->dogecoin_withdrawal_comission}}+4 DOGE</span>
						</form>
					</div>
				</div>


				<div class="panel panel-default">
					<div class="panel-body">
						<h4>@lang('crypto.latest_transactions')</h4>
						<p>@lang('crypto.latest_transactions_info')</p>

						@if(count($model) > 0)

							@foreach($model as $single)
								@php
									$ext = 'BTC';
									if($single->network == "Litecoin") {
										$ext = 'LTC';
									}elseif($single->network == "Dogecoin"){
										$ext = 'DOGE';
									}
								@endphp
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-2 text-center">

												@if($single->type == "sent")
													<span class="text text-danger text-center"><i class="fa fa-arrow-circle-o-up fa-2x"></i><br/>@lang('crypto.sent')</span>
												@else
													<span class="text text-success text-center"><i class="fa fa-arrow-circle-o-down fa-2x"></i><br/>@lang('crypto.received')</span>
												@endif

												<br><br>
												<span class="text-muted"><small>{{$single->confirmations}}  @lang('crypto.confirmations')</small></span>
											</div>
											<div class="col-md-10">
												<table class="table table-striped">
													<tbody>
														<tr>
															<td>@lang('crypto.transaction'):</td>
															<td><a href="https://chain.so/tx/{{$ext}}/{{$single->txid}}">
																	@php
																		$string = $single->txid;
																		if(strlen($string) > 30) {
																			$string = substr($string, 0, 30).'...';
																			echo $string;
																		} else {
																			echo $string;
																		}
																	@endphp
																</a>
															</td>
														</tr>
														<tr>
															<td>@lang('crypto.sender'):</td>
															<td>{{$single->sender}}</td>
														</tr>
														<tr>
															<td>@lang('crypto.recipient'):</td>
															<td>{{$single->recipient}}</td>
														</tr>
														<tr>
															<td>@lang('crypto.amount'):</td>
															<td>{{$single->amount}} {{$ext}}</td>
														</tr>
														<tr>
															<td>@lang('crypto.time'):</td>
															<td>{{date('d/m/Y H:i',$single->time)}}</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@else
							<div class="alert alert-info"><i class="fa fa-info-circle"></i> @lang('crypto.info_9')</div>
						@endif


					</div>
				</div>


			</div>
		</div>
	</div>


@endsection