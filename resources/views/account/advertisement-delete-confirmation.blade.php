@extends('layouts.master')
@section('content')
<!-- Page Title-->
	<div class="container-fluid blue-banner page-title bg-image">
	</div>
<!-- Page Title-->

	<div class="container ex_padding" style="padding-top:20px;padding-bottom:20px;font-size:15px;">
		<div class="row">
			<div class="col-md-3">
				@include('account.accordion-menu')
			</div>
			<div class="col-md-9">
				<div class="panel panel-default">
					<div class="panel-body">
						<h4>@lang('crypto.menu_advertisements') <small>@lang('crypto.delete')</small></h4>
						<hr/>
						@if($action == 'confirmation')
							<div class="alert alert-info"><i class="fa fa-info-circle"></i> @lang('crypto.info_4') <b>#{{$id}}?</b>
							</div>
							<a href="{{route('account.advertisementDelete',$id)}}" class="btn btn-success"><i class="fa fa-check"></i> @lang('crypto.yes')</a>
							<a href="{{route('account.advertisement')}}" class="btn btn-danger"><i class="fa fa-times"></i> @lang('crypto.no')</a>
						@elseif($action == 'deleted')
							@php
								$lang_ad_deleted = str_ireplace("%ad_id%",$id,__('crypto.ad_deleted'));
							@endphp
							<div class="alert alert-success"><i class="fa fa-check"></i> {!! $lang_ad_deleted !!}</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection