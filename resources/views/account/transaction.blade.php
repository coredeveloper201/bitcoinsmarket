@extends('layouts.master')
@section('content')
     <!-- Page Title-->
    	<div class="container-fluid blue-banner page-title bg-image">
		 
        </div>
    <!-- Page Title-->
	<div class="container ex_padding" style="padding-top:20px;padding-bottom:20px;font-size:15px;">
		<div class="row">
			<div class="col-md-3">
				@include('account.accordion-menu')
			</div>
			<div class="col-md-9">
			
				<div class="panel panel-default">
					<div class="panel-body">
						<h4>@lang('crypto.menu_transactions')</h4>
						<hr/>

						@if(count($model) > 0)
							@foreach($model as $single)
								@php
									$ext = 'BTC';
									if($single->network == "Litecoin") {
										$ext = 'LTC';
									}elseif($single->network == "Dogecoin"){
										$ext = 'DOGE';
									}
								@endphp

								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-2 text-center">
												@if($single->type == "sent")
													<span class="text text-danger text-center"><i class="fa fa-arrow-circle-o-up fa-2x"></i><br/>@lang('crypto.sent')</span>
												@else
													<span class="text text-success text-center"><i class="fa fa-arrow-circle-o-down fa-2x"></i><br/>@lang('crypto.received')</span>
												@endif

												<br><br>
												<span class="text-muted"><small>{{$single->confirmations}}  @lang('crypto.confirmations')</small></span>
											</div>
											<div class="col-md-10">
												<table class="table table-striped">
													<tbody>
														<tr>
															<td>@lang('crypto.transaction'):</td>
															<td><a href="https://chain.so/tx/{{$ext}}/{{$single->txid}}">
																@php
																	$string = $single->txid;
																	if(strlen($string) > 30) {
																		$string = substr($string, 0, 30).'...';
																		echo $string;
																	} else {
																		echo $string;
																	}
																@endphp
															</a>
														</td>

														<tr>
															<td>@lang('crypto.sender'):</td>
															<td>{{$single->sender}}</td>
														</tr>
														<tr>
															<td>@lang('crypto.recipient'):</td>
															<td>{{$single->recipient}}</td>
														</tr>
														<tr>
															<td>@lang('crypto.amount'):</td>
															<td>{{$single->amount}} {{$ext}}</td>
														</tr>
														<tr>
															<td>@lang('crypto.time'):</td>
															<td>{{date('d/m/Y H:i',$single->time)}}</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							@endforeach
						@else
							<div class="alert alert-info"><i class="fa fa-info-circle"></i> @lang('crypto.info_7')</div>
						@endif

					</div>
				</div>
			
			</div>
		</div>
	</div>

@endsection