@extends('layouts.master')
@section('content')

    <div class="container-fluid page-title">
			<div class="row blue-banner">
            	<div class="container main-container">
                	<div class="col-lg-12 col-md-12 col-sm-12">
                		<h3 class="white-heading">@lang('crypto.contact')</h3>
                    </div>
                </div>
            </div> 
        </div> 
  	 <!--header section -->
	 <!-- full width section -->
    	<div class="container-fluid white-bg">
        	<div class="row">
            	<div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
							<div class="row">
						<div class="col-md-12">
							@include('flash-message')
						</div>
						<div class="col-md-12">
							<form action="{{route('sendMessage')}}" method="POST">
								{{csrf_field()}}
								<div class="form-group">
									<label>@lang('crypto.your_name')</label>
									<input type="text" class="form-control" name="name">
								</div>	
								<div class="form-group">
									<label>@lang('crypto.your_email')</label>
									<input type="text" class="form-control" name="email">
								</div>	
								<div class="form-group">
									<label>@lang('crypto.subject')</label>
									<input type="text" class="form-control" name="subject">
								</div>	
								<div class="form-group">
									<label>@lang('crypto.message')</label>
									<textarea class="form-control" name="message" rows="3"></textarea>
								</div>	
								<button type="submit" class="btn btn-primary" name="bit_send">@lang('crypto.btn_send_message')</button>
							</form>
						</div>
					</div>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection