@extends('layouts.master')
@section('content')

   			<!--header section -->
    	<div class="container-fluid page-title">
			<div class="row blue-banner">
            	<div class="container main-container">
                	<div class="col-lg-12 col-md-12 col-sm-12">
                		<h3 class="white-heading">@lang('crypto.faq')</h3>
                    </div>
                </div>
            </div>
        </div>
  	 <!--header section -->
	 <div class="container-fluid white-bg">
        	<div class="row">
            	<div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
                            <div class="row">
								@if(count($model) > 0)
									@foreach($model as $single)
										<div class="col-md-12">
											<h4>Q? {{$single->question}}</h4>
											<h5>A: {{$single->answer}}</h4>
										</div>
									@endforeach
								@else
									No data for display.
								@endif
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection