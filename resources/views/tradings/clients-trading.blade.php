<div class="panel panel-default">
    <div class="panel-body">
        <p style="font-size:16px;">
            @php
                $lang_trade_info_12 = str_ireplace("%payment_method%",adinfo($model->ad_id,"payment_method"),__('crypto.trade_info_12'));
                echo $lang_trade_info_12;
            @endphp
        </p>
        <p style="font-size:15px;">
            @php
                $lang_trade_info_13 = str_ireplace("%payment_hash%",$model->payment_hash,__('crypto.trade_info_13'));
                echo $lang_trade_info_13;
            @endphp
        </p>
        <p style="font-size:14px;" class="text text-danger">@lang('crypto.trade_info_14')</p>
        <p>@lang('crypto.trade_info_3')</p>
        <br>

        <form action="{{route('account.processTrade')}}" method="POST">
            <input type="hidden" name="trade_id" value="{{$model->id}}">
            <input type="hidden" name="minutes" value="{{$minutes}}">
            {{csrf_field()}}
            @if($model->status < 3)
                @if($model->released_bitcoins == "0")
                    @if($minutes !== "0")
                        <button type="submit" class="btn btn-success" name="btc_release_bitcoins"><i class="fa fa-check"></i> @lang('crypto.btn_release_bitcoins')</button>
                        <button type="submit" class="btn btn-danger" name="btc_cancel_trade" id="btc_cancel_trade"><i class="fa fa-times"></i> @lang('crypto.btn_cancel_trade')</button>
                    @endif
                @endif
            @endif
            <a href="{{route('account.tradeReportForm',$model->id)}}" class="btn btn-warning"><i class="fa fa-flag"></i> @lang('crypto.btn_report_trade')</a>

            @if($model->status == "7")
                @if(!checkFeedback($model->id))
                    <a href="{{route('leaveFeedbackFrom',$model->id)}}" class="btn btn-info"><i class="fa fa-comment"></i> @lang('crypto.btn_leave_feedback')</a>
                @endif
            @endif
        </form>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">

        @if($minutes == 0)
            @lang('crypto.trade_info_4')
        @else
            @php
                $lang_trade_info_8 = str_ireplace("%minutes%",$minutes,__('crypto.trade_info_8'));
                echo $lang_trade_info_8;
            @endphp

        @endif

    </div>
</div>