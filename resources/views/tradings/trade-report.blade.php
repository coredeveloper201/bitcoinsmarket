@extends('layouts.master')
@section('content')

<div class="container-fluid page-title">
			<div class="row blue-banner">
            	<div class="container main-container">
                	<div class="col-lg-12 col-md-12 col-sm-12">
					<h3 class="white-heading">@lang('crypto.report_trade')</h3>
					</div>
                </div>
            </div>
        </div>
  	 <!--header section -->

	<!-- full width section -->
    	<div class="container-fluid white-bg">
        	<div class="row">
            	<div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="content">
						<div class="container">
							<div class="col-md-12" style="margin-top:10px;margin-bottom:30px;"	>
								<div class="row">
									<div class="col-md-12">
										<span style="font-size:35px;font-weight:bold;">@lang('crypto.ttrade') #{{$model->id}} <small>@lang('crypto.from_advertisement') <a href="" >#{{$model->ad_id}}</a>, @lang('crypto.bitcoin_price') {{$model->crypto_price}} {{ adinfo($model->ad_id,'currency')}}/BTC</span><br/>
										<span style="font-size:25px;">@lang('crypto.trade_amount'): <b> {{$model->amount}} {{adinfo($model->ad_id,'currency')}} </b> ({{$model->crypto_amount}} )</span>
									</div>
								</div>
							</div>
						</div>
					<br>

					@include('flash-message')

					<form action="{{route('account.tradeReportSave',$model->id)}}" method="POST">
						{{csrf_field()}}
						<div class="form-group">
							<label>@lang('crypto.your_report')</label>
							<textarea class="form-control" name="content" rows="10"></textarea>
						</div>
						<button type="submit" class="btn btn-primary" name="btc_report"><i class="fa fa-check"></i> @lang('crypto.btn_submit')</button>
					</form>
					</div>
					</div>
				</div>
			</div>
		</div>

@endsection