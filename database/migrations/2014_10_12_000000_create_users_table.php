<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('secret_pin');
            $table->string('email')->nullable();
            $table->tinyInteger('email_verified')->nullable();
            $table->text('email_hash')->nullable();
            $table->tinyInteger('document_verified')->nullable();
            $table->text('document_1')->nullable();
            $table->text('document_2')->nullable();
            $table->tinyInteger('mobile_verified')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('status')->nullable();
            $table->string('	hash')->nullable();
            $table->string('	ip')->nullable();
            $table->integer('time_signup')->nullable();
            $table->integer('time_signin')->nullable();
            $table->integer('time_activity')->nullable();
            $table->integer('referral_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
